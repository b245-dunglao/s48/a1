
let post = [];
// post will serve as our mock database. 
	// array of objects
		/*
			{
				id:value,
				title:value,
				body: value

			}
		*/

let count = 1; 
//add post data
document.querySelector(`#form-add-post`).addEventListener("submit",(event)=>{
	// preventDefault() function stops the autoreload of the webpage when submitting or onclick of the button. 
	event.preventDefault();

	let newPost = {
		id: count,
		title: document.querySelector(`#txt-title`).value,
		body: document.querySelector('#txt-body').value
		
	}

	console.log(newPost);
	post.push(newPost);
	console.log(post);
	count++;

	showPost(post);

		document.querySelector("#txt-title").value = "";
		document.querySelector("#txt-body").value = "";

})


// show post 

	const showPost = (post) => {
		let postEntries =``;

		post.reverse();
		post.forEach((post)=>{
			postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>	
				<p><button onClick="editPost(${post.id})">Edit</button><button onClick="deletePost(${post.id})">Delete</button></p>
			</div>`
		})

		document.querySelector(`#div-post-entries`).innerHTML = postEntries;


	}

	

	// editpost

	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;
		
		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;


	}


	// activity
	// deletePost

	const deletePost = (id) =>{
		// let index = post.indexOf(id);
		// post.pop(index)

		// post.splice(post.findIndex(index => index.id === id),1);

		post = post.filter((post)=>(post.id != id));
		document.getElementById(`post-${id}`).remove();

		// showPost(post);


	}



	// edit save
	document.querySelector(`#form-edit-post`).addEventListener("submit",(event)=>{
			event.preventDefault();

			// forEach to check/find the document to be edited

			post.forEach(posts => {
				let idToBeEdited = document.querySelector("#txt-edit-id").value;

				if(posts.id == idToBeEdited){
						
					let title = document.querySelector(`#txt-edit-title`).value;
					let body = document.querySelector('#txt-edit-body').value;
					
					post[posts.id-1].title = title;
					post[posts.id-1].body = body;
					alert("Edit is successful!");
					showPost(post);

					document.querySelector("#txt-edit-title").value = "";
					document.querySelector("#txt-edit-body").value = "";
					document.querySelector("#txt-edit-id").value = "";



				}
			})


	})


	



